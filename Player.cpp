#include "Player.h"
#include "utils.h"
#include "Point.h"
#include <iostream>

Player::Player(float x1, float y1, float x2, float y2, 
				float x3, float y3, float x4, float y4){
	obb = OBB(x1, y1, x2, y2, x3, y3, x4, y4);
	vertices[0] = obb.vertOriginal[0];
	vertices[1] = obb.vertOriginal[1];
	vertices[2] = obb.vertOriginal[2];
	vertices[3] = obb.vertOriginal[3];
	texture = loadPNG("./texture/player.png");
	rotate = 0;
	life = 100;
	brake = 1;
	speed = 0.01f;
	player_cood = Point();		//starts at origin in world
}

void Player::display(){
	//drawing the player rectangle with the player texture on top and OBB
	float matrix[16];
	glColor3f(0.5, 0.5, 0.5);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
	glPushMatrix();

	// player movement
	glTranslatef(player_cood.x, player_cood.y, 0.0);
	glRotatef(rotate, 0.0, 0.0, 1.0);

	glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
	
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0); glVertex2f(vertices[0].x, vertices[0].y);
		glTexCoord2f(0.0, 1.0); glVertex2f(vertices[1].x, vertices[1].y);
		glTexCoord2f(1.0, 1.0); glVertex2f(vertices[2].x, vertices[2].y);
		glTexCoord2f(1.0, 0.0); glVertex2f(vertices[3].x, vertices[3].y);
	glEnd();
	glColor4f(0, 0, 0, 0);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	obb.transformPoints(matrix);
	
	move(brake * speed);
}

void Player::take_damage(int n) {
	life -= n;
	PlaySound(TEXT("texture/Crash.wav"), NULL, SND_SYNC | SND_FILENAME);
}

void Player::move(float velocity){
	float angle = (90 + rotate) * (PI / 180.0f);
	player_cood.x += velocity * cosf(angle);
	player_cood.y += velocity * sinf(angle);
}

void Player::turn(float a){
	fmod(rotate += 5 * a, 360);
}

#pragma region Enemy

Enemy::Enemy(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4){
	obb = OBB(x1, y1, x2, y2, x3, y3, x4, y4);
	vertices[0] = obb.vertOriginal[0];
	vertices[1] = obb.vertOriginal[1];
	vertices[2] = obb.vertOriginal[2];
	vertices[3] = obb.vertOriginal[3];
	texture = loadPNG("./texture/enemy.png");
	speed = 0.01f;
	rotate = 0;
	enemy_cood = Point(0, -15);
	path = { Point(0,20), Point(0,108), Point(112,108), 
			 Point(113,118), Point(114,118), Point(115,118), Point(116,118), 
			 Point(116,130), Point(116,225), Point(-200,225), Point(-200,332)};
	it = path.begin();
}

// https://stackoverflow.com/questions/43164540/draw-trajectory-of-an-object-in-opengl
void Enemy::display(){
	// instruct the enemy to move on the path points
	if (it != path.end()) {
		float dx = it->x - enemy_cood.x;	// find the difference in coordinate
		float dy = it->y - enemy_cood.y;
		float length = sqrt(dx * dx + dy * dy);
		float x = -atan2f(dx, dy) * (180.0f / PI);	// calculate the angle of roation to point to the target
		rotate = x;

		// regain speed if previously crashed
		if (speed < 0.01f) {
			speed += 0.00000001f;
		}

		// move to towards the point
		if (length > 1.0f) {
			enemy_cood.x += speed * cosf((90 + rotate) * (PI / 180.0f));
			enemy_cood.y += speed * sinf((90 + rotate) * (PI / 180.0f));
		}
		else {
			it++;		// proceed to next point
		}
	}
	
	//drawing the player rectangle with the player texture on top and OBB
	float matrix[16];
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
	glPushMatrix();
	glTranslatef(enemy_cood.x, enemy_cood.y, 0.0);
	glRotatef(rotate, 0.0, 0.0, 1.0);
	glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0); glVertex2f(vertices[0].x, vertices[0].y);
		glTexCoord2f(0.0, 1.0); glVertex2f(vertices[1].x, vertices[1].y);
		glTexCoord2f(1.0, 1.0); glVertex2f(vertices[2].x, vertices[2].y);
		glTexCoord2f(1.0, 0.0); glVertex2f(vertices[3].x, vertices[3].y);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	obb.transformPoints(matrix);
}

void Enemy::move(float velocity){
	float angle = (90 + rotate) * (PI / 180.0f);
	enemy_cood.x += velocity * cosf(angle);
	enemy_cood.y += velocity * sinf(angle);
}

void Enemy::turn(float a){
	fmod(rotate += a, 360);
}
#pragma endregion