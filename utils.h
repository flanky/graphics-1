#pragma once

#include <windows.h>
#include <math.h>
#include "Image_Loading/nvImage.h"
#include "GL\freeglut.h"
// #include "Image_Loading/glew.h"
#include "OBB.h"

using namespace std;
const float PI = 3.1415926535897932384626433832795f;
float sqdist(float x1, float x2, float y1, float y2);
float sqdist(float x);
float rotating_point(float x, float angle, float offset = 0);
void drawCircle(float x, float y, float radius);
bool circle_collide(float x1, float x2, float y1, float y2, float r1, float r2);
bool axis_align_collide(float xMinA, float xMaxA, float xMinB, float xMaxB, 
						float yMinA, float yMaxA, float yMinB, float yMaxB);
GLuint loadPNG(char* name);

