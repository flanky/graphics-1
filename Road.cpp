#include "Road.h"

Road::Road(){
	// connect all the tiles together
	tiles.emplace_back(Type::VERTICAL, -8,-20, -8,100, 8,100, 8,-20);
	tiles.emplace_back(Type::FRONT_LEFT, -8,100, -8,116, 8,116, 8,100);
	tiles.emplace_back(Type::HORIZONTAL, 8,100, 108,100, 108,116, 8,116);
	tiles.emplace_back(Type::FRONT_RIGHT, 108,100, 124,100, 124,116, 108,116);
	tiles.emplace_back(Type::VERTICAL, 108,116, 108,216, 124,216, 124,116);
	tiles.emplace_back(Type::BACK_RIGHT, 124,216, 124,232, 108,232, 108,216);
	tiles.emplace_back(Type::HORIZONTAL, -192,216, 108,216, 108,232, -192,232);
	tiles.emplace_back(Type::BACK_LEFT, -208,232, -208,216, -192,216, -192,232);
	tiles.emplace_back(Type::VERTICAL, -208,232, -208,332, -192,332, -192,232);
}

void Road::display(){
	// display the whole road
	for(int i=0; i<tiles.size() ;i++)
		tiles[i].display();
	
	glClearColor(0.0, 0.0, 0.0, 1.0);
}

Tile::Tile(Type t, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4){
	type = t;
	passed = false;
	vertices[0] = Point(x1, y1);
	vertices[1] = Point(x2, y2);
	vertices[2] = Point(x3, y3);
	vertices[3] = Point(x4, y4);

	//loads the corresponding vertices for the left and right OBB and score line and texture
	switch (type){
		case VERTICAL:
			texture = loadPNG("./texture/straight_road.png");
			obbl = OBB(vertices[0].x - 1, y1, vertices[1].x - 1, y2, vertices[0].x - 1 - 2, y3, vertices[1].x - 1 - 2, y4);
			obbr = OBB(vertices[2].x + 1, y1, vertices[3].x + 1, y2, vertices[2].x + 1 + 2, y3, vertices[3].x + 1 + 2, y4);
			score_line = OBB(vertices[1].x,vertices[1].y, vertices[2].x,vertices[1].y, 
							 vertices[2].x,vertices[2].y - 0.1, vertices[1].x,vertices[2].y - 0.1);
			break;
		case HORIZONTAL:
			texture = loadPNG("./texture/straight_road.png");
			obbl = OBB(vertices[0].x, y3+1, vertices[0].x, y3 + 1 + 2, vertices[1].x, y3 + 1 + 2, vertices[1].x, y3+1);
			obbr = OBB(vertices[0].x, y1-1, vertices[0].x, y1 - 1 - 2, vertices[1].x, y1 - 1 - 2, vertices[1].x, y1-1);
			score_line = OBB(vertices[1].x, vertices[1].y, vertices[2].x, vertices[2].y,
							 vertices[2].x - 0.1, vertices[2].y, vertices[2].x - 0.1, vertices[1].y);
			break;
		case FRONT_LEFT:
			texture = loadPNG("./texture/curved_road_min.png");
			obbl = OBB(vertices[0].x, y1, vertices[0].x - 2, y1, vertices[0].x-2, y3, vertices[0].x, y3);
			obbr = OBB(vertices[0].x, y2, vertices[1].x, y2 + 1 + 2, vertices[2].x , y3 + 1 + 2, vertices[3].x, y3);
			score_line = OBB(vertices[2].x, vertices[2].y, vertices[3].x, vertices[3].y,
							 vertices[3].x - 0.1, vertices[3].y, vertices[3].x - 0.1, vertices[2].y);
			break;
		case FRONT_RIGHT:
			texture = loadPNG("./texture/curved_road_min.png");
			/*obbl = OBB(vertices[0].x, y1-1, vertices[0].x, y1-1-2, vertices[3].x, y1-1-2, vertices[3].x - 2, y1-1);
			obbr = OBB(vertices[3].x+1, y4, vertices[3].x+1, y3, vertices[3].x +1+2, y3, vertices[3].x+1+2, y4);*/
			obbl = OBB(vertices[0].x, y1 - 1, vertices[0].x, y1 - 1 - 2, vertices[1].x, y1 - 1 - 2, vertices[1].x, y1 - 1);
			obbr = OBB(vertices[1].x + 1, y2, vertices[1].x + 1, y3, vertices[1].x + 1 + 2, y3, vertices[1].x + 1 + 2, y2);
			score_line = OBB(vertices[2].x, vertices[2].y, vertices[3].x, vertices[3].y,
							 vertices[3].x , vertices[3].y - 0.1, vertices[3].x , vertices[2].y - 0.1);
			break;
		case BACK_LEFT:
			texture = loadPNG("./texture/curved_road_min.png");
			obbl = OBB(vertices[1].x - 1, y2,  vertices[1].x - 1 - 2, y2, vertices[1].x - 1 - 2, y1, vertices[1].x - 1, y1);
			obbr = OBB(vertices[1].x, y2 - 1, vertices[1].x, y2 -1 -2, vertices[2].x, y2 -1 -2, vertices[2].x, y2 - 1);
			score_line = OBB(vertices[2].x, vertices[2].y, vertices[3].x, vertices[3].y,
							 vertices[3].x - 0.1, vertices[3].y , vertices[3].x - 0.1, vertices[2].y);
			break;
		case BACK_RIGHT:
			texture = loadPNG("./texture/curved_road_min.png");
			obbl = OBB(vertices[0].x, y2, vertices[1].x, y2 + 1 + 2, vertices[2].x, y3 + 1 + 2, vertices[3].x, y3);
			obbr = OBB(vertices[0].x, y1, vertices[0].x + 2, y1, vertices[0].x + 2, y3, vertices[0].x, y3);
			score_line = OBB(vertices[2].x, vertices[2].y, vertices[3].x, vertices[3].y,
							 vertices[3].x + 0.1, vertices[3].y, vertices[3].x + 0.1, vertices[2].y);
			break;
		default:
			break;
	}
	
}

void Tile::display(){
	float obb_matrix[16];

	// left bound
	glColor4f(0.0, 1, 0.0, 0.0);
	glBegin(GL_QUADS);
		glVertex2f(obbl.vertOriginal[0].x, obbl.vertOriginal[0].y);
		glVertex2f(obbl.vertOriginal[1].x, obbl.vertOriginal[1].y);
		glVertex2f(obbl.vertOriginal[2].x, obbl.vertOriginal[2].y);
		glVertex2f(obbl.vertOriginal[3].x, obbl.vertOriginal[3].y);
	glEnd();

	glGetFloatv(GL_MODELVIEW_MATRIX, obb_matrix);
	obbl.transformPoints(obb_matrix);

	// draw tile based on the vertices
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
	glColor3f(0.5, 0.5, 0.5);
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0); glVertex2f(vertices[0].x, vertices[0].y);
		glTexCoord2f(0.0, 1.0); glVertex2f(vertices[1].x, vertices[1].y);
		glTexCoord2f(1.0, 1.0); glVertex2f(vertices[2].x, vertices[2].y);
		glTexCoord2f(1.0, 0.0); glVertex2f(vertices[3].x, vertices[3].y);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	float score_line_matrix[16];
	glColor4f(0.0, 1, 0.0, 0.0);
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0); glVertex2f(score_line.vertOriginal[0].x, score_line.vertOriginal[0].y);
		glTexCoord2f(0.0, 1.0); glVertex2f(score_line.vertOriginal[1].x, score_line.vertOriginal[1].y);
		glTexCoord2f(1.0, 1.0); glVertex2f(score_line.vertOriginal[2].x, score_line.vertOriginal[2].y);
		glTexCoord2f(1.0, 0.0); glVertex2f(score_line.vertOriginal[3].x, score_line.vertOriginal[3].y);
	glEnd();
	glGetFloatv(GL_MODELVIEW_MATRIX, score_line_matrix);
	score_line.transformPoints(score_line_matrix);

	float bb_matrix[16];

	//right bound
	glBegin(GL_QUADS);
		glVertex2f(obbr.vertOriginal[0].x, obbr.vertOriginal[0].y);
		glVertex2f(obbr.vertOriginal[1].x, obbr.vertOriginal[1].y);
		glVertex2f(obbr.vertOriginal[2].x, obbr.vertOriginal[2].y);
		glVertex2f(obbr.vertOriginal[3].x, obbr.vertOriginal[3].y);
	glEnd();

	glGetFloatv(GL_MODELVIEW_MATRIX, bb_matrix);
	obbr.transformPoints(bb_matrix);
	glClearColor(0, 0, 0, 1);
}
