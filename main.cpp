
#include "main.h"
#include <iostream>
// ref https://clipartart.com/categories/top-view-of-a-car-clipart.html
// https://www.pngfind.com/mpng/iiJmi_free-png-download-police-car-png-top-view/

float const LEFT = 1;				//some constant to show direction 
float const RIGHT = -1;
float const FORWARD = 1;
float const BACKWARD = -1;
float prev_angle;
int	mouse_x=0, mouse_y=0;
int screenWidth=480, screenHeight=480;
bool keys[256];						// flags showing the status of all the keys
GLuint bg;							// background texture id
GLuint tunnel;						// endpoint texture id
bool collided = false;				// a flag raise when player and the road boundary collide
bool vehicle_collided = false;		// a flag raise when player and enemy collide
bool slow_down = false;
bool start = false;					// indicate whether the game starts or not
bool finish = false;				// indicate whether the game finishes or not
bool win = false;					// indicate whether the player win or lose
int score = 0;						// player score
Player *player;
Enemy* enemy;
Road* road;
OBB endpoint;

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();

	if (!start)
		start_screen();
	else if (finish)
		end_screen(win);
	else {
		background();
		glPushMatrix();
		//camera follows player
		glRotatef(-player->rotate, 0.0, 0.0, 1.0);
		glTranslatef(-player->player_cood.x, -player->player_cood.y, 0.0);

		//draw road
		road->display();
		//draw the player
		player->display();
		//draw AI
		enemy->display();
		//draw end point
		float matrix[16];
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, tunnel);
		glBegin(GL_QUADS);
			glTexCoord2f(0.0, 0.0); glVertex2f(endpoint.vertOriginal[0].x, endpoint.vertOriginal[0].y);
			glTexCoord2f(0.0, 1.0); glVertex2f(endpoint.vertOriginal[1].x, endpoint.vertOriginal[1].y);
			glTexCoord2f(1.0, 1.0); glVertex2f(endpoint.vertOriginal[2].x, endpoint.vertOriginal[2].y);
			glTexCoord2f(1.0, 0.0); glVertex2f(endpoint.vertOriginal[3].x, endpoint.vertOriginal[3].y);
		glEnd();
		glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
		endpoint.transformPoints(matrix);
		glDisable(GL_TEXTURE_2D);

		glPopMatrix();

		//dsiplay score and life info
		glRasterPos2f(10.8, 18.5);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)"Score: ");
		glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)std::to_string(score).c_str());

		glRasterPos2f(10.8, 16.5);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)"Life: ");
		glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)std::to_string(player->life).c_str());

		//collision detection
		collision();

		// game ends when player dies
		if (player->life <= 0)
			finish = true;

		//if player reach end point win game
		if (player->obb.SAT2D(endpoint)) {
			score += 500;
			win = true;
			finish = true;
			Sleep(300);
		}
		// if enemy reach endpoint instead game lose
		else if (enemy->obb.SAT2D(endpoint)) {
			finish = true;
			Sleep(300);
		}
		
	}

	glFlush();
	glutSwapBuffers();
}

void collision() {
	// when player crash into enemy
	if (player->obb.SAT2D(enemy->obb)) {
		// player take damage deduct score
		score -= 10;
		player->take_damage();
		
		if (player->speed == 0)
			player->move(50);
		else
			player->move(100 * player->speed);
		enemy->move(-100*enemy->speed);

		// make both vehicle to stop after the push back
		player->speed = 0;
		enemy->speed = 0;
		vehicle_collided = true;
	}
	else if (enemy->speed <= 0.01f && vehicle_collided){
		// reset the enemy speed and close the colliding flag
		enemy->speed = 0.001f;
		vehicle_collided = false;
	}
	
	
	// check each tile bound with the player collision
	for (int i = 0; i < road->tiles.size(); i++) {
		if (player->obb.SAT2D(road->tiles[i].score_line) && !road->tiles[i].passed) {
			score += 100;		// award score when player first passes the tile
			road->tiles[i].passed = true;
		}

		if (player->obb.SAT2D(road->tiles[i].obbl) || player->obb.SAT2D(road->tiles[i].obbr)) {
			if (!collided) {
				score -= 10;
				collided = true;
				player->take_damage();
			}
			// make player go in reverse direction
			player->speed = 100 * -player->speed;
			player->move(player->speed);
			// turn the angle in reverse direction
			if (prev_angle < player->rotate)
				player->rotate += player->speed;
			else if (prev_angle > player->rotate)
				player->rotate -= player->speed;
		}
		else {
			// restore speed after free from colliding
			if (collided) {
				player->speed = 0.01f;
			}
			collided = false;
		}
	}
	prev_angle = player->rotate;		// keep track of the player's turning
}

void background() {
	// display square that covers the entire viewport
	glColor3f(1.0, 1.0, 1.0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, bg);
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0); glVertex2f(-20, -20);
		glTexCoord2f(0.0, 1.0); glVertex2f(-20, 20);
		glTexCoord2f(1.0, 1.0); glVertex2f(20, 20);
		glTexCoord2f(1.0, 0.0); glVertex2f(20, -20);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

void start_screen() {
	//display blue square background
	glPushMatrix();

	glColor3f(0.15, 0.325, 0.83);
	glBegin(GL_QUADS);
		glVertex2f(-15, -15);
		glVertex2f(-15, 15);
		glVertex2f(15, 15);
		glVertex2f(15, -15);
	glEnd();

	glPopMatrix();

	// writes introduction text
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glRasterPos2f(-12, 5);
	glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)"You as a spy have just been "
																	"\ndiscovered and being chased down"
																	"\nby the gang you spy on "
																	"\n\nget rid of them before they catch you"
																	"\nwithout dying!");

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glRasterPos2f(-7, -8);
	glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)"Press any key to start");
	
}

void end_screen(bool win) {
	// display square with color depending on whether player win or not
	glPushMatrix();
	if (win)
		glColor3f(0.15, 0.325, 0.83);		//blue background for victory
	else
		glColor3f(1.0, 0.1, 0.1);			//red background for loss
	glBegin(GL_QUADS);
		glVertex2f(-15, -15);
		glVertex2f(-15, 15);
		glVertex2f(15, 15);
		glVertex2f(15, -15);
	glEnd();

	// writes the corresponding information on top of it
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glRasterPos2f(-5, 5);
	if(win)
		glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)"You win!!!!\n\nScore: ");
	else
		glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)"You lose!!!!\n\nScore: ");

	glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)std::to_string(score).c_str());

	// hint player can press R or X 
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glRasterPos2f(-13, -8);
	glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)"Press R to restart  \t\t\t  Press X to exit");

	glPopMatrix();
}

void reshape(int width, int height)		// Resize the OpenGL window
{
	screenWidth=width; screenHeight = height;           // to ensure the mouse coordinates match 
	// we will use these values to set the coordinate system
	
	glViewport(0,0,width,height);						
	
	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									

	gluOrtho2D(-20, 20, -20, 20);						// set the coordinate system for the window
	
	
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									
}

void init()
{
	glClearColor(0.0,0.0,0.0,1.0);						//sets the clear colour to yellow

	// initialize all the objects for the game
	player = new Player(-3, -3, -3, 8, 3, 8, 3, -3);
	enemy = new Enemy(-3, -3, -3, 8, 3, 8, 3, -3);
	road = new Road();
	endpoint = OBB(-208, 332, -208, 342, -192, 342, -192,332);
	bg = loadPNG("./texture/background.png");
	tunnel = loadPNG("./texture/tunnel.png");
}

void keyfunction(unsigned char key, int x, int y) {
	if (start && !finish) {		// game is going on
		if (key == 'w') {
			keys['W'] = true;
			slow_down = false;
		}
		if (key == 'a') {
			keys['A'] = true;
		}
		if (key == 's') {
			keys['S'] = true;
		}
		if (key == 'd') {
			keys['D'] = true;
		}
		if (key == 'b') {
			keys['B'] = true;
		}
	}
	else if (!start) {
		start = true;		// set the game to start regardless the key
	}
	else if (finish) {		// at the endscreen
		if (key == 'x')
			exit(0);		// X for quit game
		if (key == 'r') {
			init();			// R for restart
			finish = false;
			start = true;
		}

	}
}

void keyup(unsigned char key, int x, int y) {
	//turn off the flags in keys if key is released
	if (key == 'w') {
		keys['W'] = false;
		slow_down = true;		// start to slow down the player
	}
	if (key == 'a') {
		keys['A'] = false;
	}
	if (key == 's') {
		keys['S'] = false;
		if (!keys['W'])			//stops the player when player stops moving back
			player->speed = 0;
	}
	if (key == 'd') {
		keys['D'] = false;
	}
	if (key == 'b') {
		keys['B'] = false;
	}
	
}

void processkeys() {
	if (keys['W']) {
		player->speed += 0.000001f;		// player accelerates
	}
	if (keys['A']) {
		player->turn(LEFT * player->speed);
	}
	if (keys['S']) {
		player->speed -= 0.000001f;		// player decelerates/ move backwards
	}
	if (keys['D']) {
		player->turn(RIGHT * player->speed);
	}
	if (keys['B']) {
		player->brake = 0.1f;			// reduce brake to decrease the player velocity
	}
	if (slow_down && player->speed > 0) {				// player vehicle decelerates gradually when slowing down
		player->speed -= (0.001f * player->speed);		// until reaches a certain threshold
		if (player->speed < 0.000001f)
			player->speed = 0;
	}
}

void special(int key, int x, int y)
{
	switch(key)
	{
		case GLUT_KEY_LEFT:

			break;
		case GLUT_KEY_RIGHT:

			break;
		case GLUT_KEY_UP:

			break;
		case GLUT_KEY_DOWN:

			break;
		case GLUT_KEY_SHIFT_L:
			
			break;
	}
	
}

void specialup(int key, int x, int y) {
	
}

void update()
{
	if(start && !finish)
		processkeys();		// handle keys response when game is on
	glutPostRedisplay();
}
/**************** END OPENGL FUNCTIONS *************************/
int main(int argc, char **argv)
{
	glutInit(&argc,argv);
	
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(screenWidth, screenHeight);
   	glutInitWindowPosition(100,100);
	
	glutCreateWindow("Graphics 1 coursework");
	
	init();
	
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(update);
	
	//add keyboard callback.
	glutKeyboardFunc(keyfunction);
	glutKeyboardUpFunc(keyup);
	//add callback for the function keys.
	glutSpecialFunc(special);
	glutSpecialUpFunc(specialup);
	
	glutMainLoop();
	
	return 0;
}


