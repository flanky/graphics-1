#include <cmath>
#pragma once
class Point{
public:
	float x;
	float y;
	Point(float x = 0, float y = 0);
	float dist(float x = 0, float y = 0);
	float sqdist(float x1 = 0, float y1 = 0);
	friend Point &operator +(const Point& p1, const Point& p2);
	friend Point& operator +=(Point& p1, const Point& p2);
	friend Point& operator +=(Point& p1, float d);
};

