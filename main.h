#include "utils.h"
#include "Player.h"
#include "Road.h"
#include <string> 

void display();				//called in winmain to draw everything to the screen
void reshape(int width, int height);				//called when the window is resized
void init();				//called in winmain when the program starts.
void keyfunction(unsigned char key, int x, int y);
void special(int key, int x, int y);
void update();				//called in winmain to update variables
void processkeys();			//calls actions to object when keys are active
void start_screen();		//show the starting screen
void end_screen(bool win);	//show the win or lose screen depending of the win flag
void background();			//display background
void collision();			//giving the collision detection and response on all game object