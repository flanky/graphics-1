#include "Point.h"

Point::Point(float x, float y) {
	this->x = x;
	this->y = y;
}

float Point::dist(float x, float y){
	return sqrtf(sqdist(x, y));
}

float Point::sqdist(float x1, float y1) {
	return pow(x - x1, 2.0) + pow(y - y1, 2.0);
}

Point& operator + (const Point& p1, const Point& p2){
	// TODO: insert return statement here
	return Point(p1.x + p2.x, p1.y + p2.y);
}

Point& operator += (Point& p1, const Point& p2) {
	// TODO: insert return statement here
	p1.x += p2.x; p1.y += p2.y;
	return p1;
}

Point& operator += (Point& p1, float d) {
	// TODO: insert return statement here
	Point p2 = Point(d, d);
	return p1 += p2;
}