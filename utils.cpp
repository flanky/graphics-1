#include "utils.h"


void drawCircle(float x, float y, float radius)
{
	glBegin(GL_LINE_LOOP);
	for (float i = 0; i < 360; i += 5)
	{
		float xcoord = x + radius * cosf(i * (PI / 180.0f));
		float ycoord = y + radius * sinf(i * (PI / 180.0f));
		glVertex2f(xcoord, ycoord);
	}
	glEnd();
}

float rotating_point(float x, float angle, float offset) {
	angle *= PI / 180.0f;
	return x * (cosf(angle) - sinf(angle)) + offset;
}

float sqdist(float x1, float x2, float y1, float y2) {
	return pow(x2 - x1, 2.0) + pow(y2 - y1, 2.0);
}

float sqdist(float x) {
	return pow(x, 2);
}

bool circle_collide(float x1, float x2, float y1, float y2, float r1, float r2) {
	return sqdist(x1, x2, y1, y2) < sqdist(r1 + r2);
}

bool axis_align_collide(float xMinA, float xMaxA, float xMinB, float xMaxB,
	float yMinA, float yMaxA, float yMinB, float yMaxB) {
	return (xMinA < xMaxB && xMaxA > xMinB &&
		yMinA < yMaxB && yMaxA > yMinB);
}

GLuint loadPNG(char* name){
	// Texture loading object
	nv::Image img;

	GLuint myTextureID;

	// Return true on success
	if (img.loadImageFromFile(name)){
		glGenTextures(1, &myTextureID);
		glBindTexture(GL_TEXTURE_2D, myTextureID);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(), img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	}

	return myTextureID;
}


