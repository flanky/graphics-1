#pragma once

#include "utils.h"
#include "Point.h" 
#include <stdlib.h>
#include <windows.h>
class Player{
private:
	Point vertices[4];
	GLuint texture;
public:
	int life;
	OBB obb;
	Point player_cood;
	float rotate;
	float brake;			// variable to multiply to the speed to slow down player
	float speed;
	Player(float x1 = 0, float y1 = 0, float x2 = 0, float y2 = 0,
			float x3 = 0, float y3 = 0, float x4 = 0, float y4 = 0);
	void display();
	void take_damage(int n=10);
	void move(float velocity);
	void turn(float a = 0.1f);
};

class Enemy{
private:
	Point vertices[4];
	GLuint texture;
	
public:
	OBB obb;
	Point enemy_cood;
	vector<Point> path;
	vector<Point>::iterator it;
	
	float rotate;
	float speed;
	Enemy(float x1 = 0, float y1 = 0, float x2 = 0, float y2 = 0,
			float x3 = 0, float y3 = 0, float x4 = 0, float y4 = 0);
	void display();
	void move(float velocity);
	void turn(float a = 0.1f);
};

