#pragma once
#include "utils.h"
#include "Point.h" 
#include <stdlib.h>

// https://www.nicepng.com/ourpic/u2q8a9o0e6q8u2r5_road-test-top-down-road-sprite/
enum Type { VERTICAL, HORIZONTAL, FRONT_LEFT, FRONT_RIGHT, BACK_LEFT, BACK_RIGHT };

class Tile {
private:
	GLuint texture;
	Point vertices[4];
public:
	Type type;
	OBB obbl;
	OBB obbr;
	OBB score_line;
	bool passed;
	Tile(Type t, float x1, float y1, float x2, float y2,
		float x3, float y3, float x4, float y4);
	void display();
};

class Road{
public:
	vector<Tile> tiles;
	Road();
	void display();
};