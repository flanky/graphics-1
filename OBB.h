
#ifndef OBB_H
#define OBB_H
#include "Point.h"
const int NUMVERTS = 4;

class OBB
{
public:
	OBB();
	OBB(float x1, float y1, float x2, float y2,
		float x3, float y3, float x4, float y4);
	Point vert[NUMVERTS];
	Point vertOriginal[NUMVERTS];
	void drawOBB();
	void transformPoints(float matrix[16]);
	bool SAT2D(OBB& p2);
	bool SATtest(float proj[4], float len);
};

#endif